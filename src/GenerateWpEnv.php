<?php

namespace Net7Pluggable;

###################################################################
#Script Name : GenerateWpEnv
#Description : Wp Starter .env file generator
#Author : Marco Baroncini
#Email : spartaco4404@yahoo.it
###################################################################


use Composer\Script\Event;


class GenerateWpEnv
{

  /** PROPERTIES **/

  protected $debug = FALSE;

  protected $to_ask = [
    [
      'key' => 'DB_NAME'
    ],
    [
      'key' => 'DB_USER',
      'value' => 'root',
    ],
    [
      'key' => 'DB_PASSWORD',
      'value' => 'root',
    ],
    [
      'key' => 'DB_HOST',
      'value' => '127.0.0.1',
    ],
    [
      'key' => 'DB_TABLE_PREFIX',
      'value' => 'net7_'
    ],
    [
      'key' => 'WP_HOME',
      'value' => 'http://127.0.0.1',
    ],
    [
      'key' => 'WORDPRESS_ENV',
      'value' => 'development',
      'description' => "Supported values: \"development\", \"staging\" and \"production\". It has effect on debug settings."
    ]
  ];

  protected $to_replace = [
    [
      'key' => 'WP_DEFAULT_THEME',
      'value' => 'divi'
    ]
  ];

  //composer
  protected $event;
  protected $io;
  protected $composer;
  protected $config;


  /** METHODS **/

  // class static entrypoint
  public static function run(Event $event)
  {
    new self($event);
  }


  function __construct(Event $event)
  {
    $this->composer = $event->getComposer();
    $this->io = $event->getIO();
    $this->event = $event;
    $this->config = $event->getComposer()->getConfig();
    //launch script
    $this->main();
  }

  protected function error($type = '')
  {
    switch ($type) {
      case 'abort':
        $this->io->writeError("Abort .env file automatic configuration.");
        break;
      default:
        $this->io->writeError("Error. Something went wrong");
        $this->io->writeError('abort');
        break;
    }

    return 1;
  }

  protected function find_var($key, $arr)
  {
    $i = 0;
    $value = false;
    while (!$value && $i < count($arr)) {
      if ($arr[$i]['key'] == $key)
        $value = $arr[$i]['value'];
      $i++;
    }
    return $value;
  }

  private function getEnvPath()
  {
    return $this->getBaseDir() . '/.env';
  }

  private function getEnvExamplePath()
  {
    return $this->getBaseDir() . '/.env.example';
  }

  private function getBaseDir() {
    return dirname( __DIR__ );
  }


  public function main(){
    $response = $this->io->askConfirmation("Hi, this script will ask you some info about this Wordpress application like: db credentials, home url ...\n Are you ready?\n");
    if ( ! $response )
      return $this->error('abort');


    if (realpath($this->getEnvPath())) {
      $response = $this->io->askConfirmation("Mmm ... You've already the .env file. Are you sure to continue?\n");
      if (!$response)
      return $this->error('abort');
    }

    if ( ! realpath($this->getEnvExamplePath() ) ) {
      $this->io->writeError("Ops ... You dont have the .env.example file necessary for this script.");
      return $this->error('abort');
    }

    $forLater = '';
    foreach ($this->to_ask as $question) {
      $key = $question['key'];
      $value = isset($question['value']) ? $question['value'] : '';
      $description = isset($question['description']) ? "\n" . $question['description'] : '';
      $default_text = $value ? " [Leave empty for the default: \"$value\"]" : '';
      $userval = $this->io->ask("Please insert {$key}:{$default_text}{$description}");

      if (!$userval && !$value) {
        $this->io->writeError( "Error: missing mandatory $key. Please edit .env file manually");
        return $this->error('abort');
      } elseif ($userval)
      $value = $userval;

      $to_replace[] = ['key' => $key, 'value' => $value];
      $forLater .= "$key: $value\n";
    }

    $this->io->write("Hey! I'm ready to create new config with this parameters:");
    $this->io->write($forLater);
    $response = $this->io->askConfirmation("It's all right?\n");

    if (!$response) {
      return $this->error('abort');
    }

    $envExample = file_get_contents($this->getEnvExamplePath());
    $env = $envExample;

    foreach ($to_replace as $arr) {
      $key = $arr['key'];
      $value = $arr['value'];
      $match = "/\#?$key\=?(.+)?\n/";

      $env = preg_replace($match, "{$key}={$value}\n", $env);
    }

    $check = file_put_contents($this->getEnvPath(), $env);

    if (
      $this->find_var('WORDPRESS_ENV', $to_replace) == 'development'
    ) {
      $response = $this->io->askConfirmation("We are in development environment... Can I create the db for you?\n");
      if ($response) {
        $user = $this->find_var('DB_USER', $to_replace);
        $name = $this->find_var('DB_NAME', $to_replace);
        $password = $this->find_var('DB_PASSWORD', $to_replace);
        $host = $this->find_var('DB_HOST', $to_replace);

        $query = "CREATE DATABASE IF NOT EXISTS $name";
        echo shell_exec("echo '$query' | mysql -u$user -p$password -h$host ");
      }
    }

    $this->io->write("Done!");


  }



}

